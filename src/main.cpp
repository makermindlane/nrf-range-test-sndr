#include <Arduino.h>
#include <RF24.h>
#include <SPI.h>
#include <nRF24L01.h>

#define DEBUG_SERIAL
// #define SERIAL_PRINT

#ifdef DEBUG_SERIAL
#define d_print(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define d_println(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#else
#define d_print(fmt, ...)
#define d_println(fmt, ...)
#endif

#ifdef SERIAL_PRINT
#define s_print(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define s_println(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#else
#define s_print(fmt, ...)
#define s_println(fmt, ...)
#endif

/*=================================Defines===================================*/
#define LOOP_EXEC_TIME 50

/*===============================Data structures=============================*/

/*=============================Function prototypes===========================*/
void init_nrf();

/*================================Varialbles=================================*/
// const byte address[6] = "00001";
const byte address[][6] = {"00001", "00002"};
const int LED_PIN = D8;

RF24 radio(D4, D2);

/**
 * ===================================setup====================================
 */
void setup() {
#if defined(DEBUG_SERIAL) || defined(SERIAL_PRINT)
  Serial.begin(9600);
#endif

  init_nrf();
  pinMode(LED_PIN, OUTPUT);
  d_println("setup finished.\n");
}

/**
 * ===================================loop=====================================
 */
void loop() {

  static unsigned long loop_exec_prev_time = 0;
  // if (millis() - loop_exec_prev_time >= LOOP_EXEC_TIME) {
  loop_exec_prev_time = millis();

  radio.stopListening();
  int fade_in = analogRead(A0);
  // Read analog pot val
  d_println("%lu: fade_in: %d", loop_exec_prev_time, fade_in);
  analogWrite(LED_PIN, fade_in);
  // Send this pot val to receiver node
  // if (radio.write(&fade_in, sizeof(fade_in)) == true) {
  //   d_println("%lu: sent fade_in: %d", loop_exec_prev_time, fade_in);
  // } else {
  //   d_println("%lu: Couldn't send %d value.", loop_exec_prev_time, fade_in);
  // }

  // }
}

/**
 * Init nrf
 */
void init_nrf() {
  radio.begin();
  radio.openWritingPipe(address[1]);    // 00002
  radio.openReadingPipe(1, address[0]); // 00001
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_250KBPS);
}
